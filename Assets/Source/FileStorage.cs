﻿using UnityEngine;
using System.Collections;
using System.Net;
using System;
using System.IO.Compression;
//using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Threading; //Именно это пространство имен поддерживает многопоточность
using System.Text;

public class FileStorage : IDisposable
{

    public int DownloadProgress;
    public bool DownloadComplite;
    public bool isDownload; // Если идет скачивание.
    public PathDefault Path;
    public string fileName;
    SerializableMovies InfoMovieCfg;

    public delegate void MoveUpdateFinishedContainer();

    public event MoveUpdateFinishedContainer MoveUpdateFinished;
    

    public struct PathDefault
    {
        public string phone;
        public string plugin;
        public string HttpServer;
        public string MovieInfo;




        public PathDefault(string phone, string plugin, string HttpServer)
        {
            this.phone = phone;
            //this.phone = @"D:\playerMovie\";
            this.plugin = plugin;
            this.HttpServer = HttpServer;
            this.MovieInfo = this.phone + "InfoMovie.cfg";
        }
    }
    Platform platform;
    Downloader downloader;
    public FileStorage(Platform platform)
    {
        this.platform = platform;
        //Path = new PathDefault("/sdcard/Movies/", "file:///sdcard/Movies/", "http://" + platform.host + "/films/");
        Path = new PathDefault("/storage/sdcard0/Movies/", "file:///storage/sdcard0/Movies/", "http://" + platform.host + "/films/");
        //Path = new PathDefault("D:/playerMovie/", "file:///sdcard/Movies/", "http://" + platform.host + "/films/");
        downloader = new Downloader(Path);
    }


  

    public bool isMovie(string movie)
    {
        if (File.Exists(movie))
            return true;
        else
            return false;
    }

    public void Dispose()
    {

    }

// Создаем либо получаем файл с списком фильмов на телефоне.
    string getFileInfoMovie()
    {
        var path = Path.phone + "InfoMovie.cfg";
        Debug.Log("getFileInfoMovie: " + path);

        if (!File.Exists(path))
            File.Create(path);
        else
        {
            using (StreamReader sr = File.OpenText(path))
            {
                string s = "";
                return sr.ReadToEnd();
            }
        }
        return null;
    }


    // Сверяем версии фильмов и обновляем при необходимости
    public void CheckAndUploadMovies(SerializableMovies serverMovies)
    {
        try
        {
            
            var localJson = getFileInfoMovie(); // Файл с версиями который лежит на телефоне.
            SerializableMovies localMovies = null;
            if (localJson != "")
            {
                InfoMovieCfg = serverMovies;
                localMovies = JsonUtility.FromJson<SerializableMovies>(localJson);
            }

            for (int i = 0; i < serverMovies.movies.Count; i++)
            {
                SerializableMovie localMovie = null;
                var Movie = serverMovies.movies[i];
                // Если файла с конфигами не существует - заливаем все.
                if (localJson == null || localJson == "")
                {
                    downloader.Add(Movie.path);
                    InfoMovieCfg = serverMovies;
                    var lastIndex = InfoMovieCfg.movies.Count - 1;
                    continue;
                }

                if ((localMovie = localMovies.FindKey(Movie)) != null)
                {
                    if (!localMovie.CompareVersion(Movie))
                    {
                        downloader.Add(Movie.path);
                    }
                }
                else
                    downloader.Add(Movie.path);

            }
            
            // Запускаем транзакцию.
            if (downloader.queue.Count != 0)
            {
                downloader.Start();
                new Thread(UpdateFileInfoMovie).Start();
            }
            else
            {
                if (MoveUpdateFinished != null)
                    MoveUpdateFinished();
            }


        }catch(Exception e)
        {
        //    platform.Send(e.Message + "\n" + e.StackTrace);
        }
    }

    // Проверяем загрузились ли все фильмы, если да - обновляем файл InfoMovie.cfg на телефоне. 
    // Если нет, оставляем как есть. Этакая транзакция.
    void UpdateFileInfoMovie()
    {
        try {
            while (downloader.queue.Count != downloader.queue.Complite)
            {
                Debug.Log("...");
                Thread.Sleep(500);
            }
            string json = JsonUtility.ToJson(InfoMovieCfg);
           // if(File.Exists(Path.MovieInfo))
                using (FileStream fs = File.OpenWrite(Path.MovieInfo))
                {
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    fs.Write(data, 0, data.Length);
                }
                if (MoveUpdateFinished != null)
                    MoveUpdateFinished();
         //   platform.Send("{\"act\":\"endMoviesUpdate\",\"playerId\":\"" + platform.id + "\"}");
        }
        catch (Exception e)
        {
      //      platform.Send(e.Message);
      //      platform.Send(e.StackTrace);
        }
        }



    class Downloader
    {
        public Downloader(PathDefault path)
        {
            this.path = path;
        }
        // очередь загрузки
        public class Queue
        {
            public int Count;
            public int Complite;
        }

        public class ListDownload
        {
            public string fileName;
            public string url;
            public ListDownload(string url)
            {
                this.fileName = System.IO.Path.GetFileName(url);
                this.url = url;
            }
        }

        public Queue queue = new Queue();
        public List<ListDownload> list = new List<ListDownload>();
        public PathDefault path;
        public void Add(string uri)
        {
            list.Add(new ListDownload(uri));
            queue.Count++;
        }

        public void Start()
        {
           
            new Thread(ThreadDownload).Start();
            //Platform.instance.Send("Download Start");
        }

        public void ThreadDownload()
        {
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    Uri url = new Uri(path.HttpServer);
                    WebClient web = new WebClient();
                    var currentFile = list[i];
                    Debug.Log(path.HttpServer + currentFile.url);
                    web.DownloadFile(path.HttpServer + currentFile.url, path.phone + currentFile.fileName);
                    queue.Complite++;
              //      Platform.instance.Send("DOWNLOAD: " + queue.Count + "/" + queue.Complite);
                }
            }catch(Exception e)
            {
                //.instance.Send("Download error: " + e.Message + "\n" + e.StackTrace);
            }
        }
    }
}



[System.Serializable]
public class SerializableMovies
{
    public List<SerializableMovie> movies;

    public SerializableMovie FindKey(SerializableMovie m)
    {
        for(int i = 0; i < movies.Count; i++)
        {
            if(movies[i].key == m.key)
            {
                return movies[i];
            }
        }
        return null;
    }
}

[System.Serializable]
public class SerializableMovie
{
    public string name;
    public string path;
    public int version;
    public string key;

    // false - версии не сходятся, true - обновление не требуется
    public bool CompareVersion(SerializableMovie m)
    {
        if (version != m.version)
            return false;
        else return true;
    }

}

