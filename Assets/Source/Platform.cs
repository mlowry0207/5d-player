﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.IO;
using MiniJSON;
using System.Collections.Generic;
using fotonotaUtils;
using System.Timers;

public class Platform : MonoBehaviour
{
    FileStorage storage;
    public GameObject EmptyVideoObject;
    public GameObject VideoObject;
    public GameObject MainCamera;
    byte[] dataRead = new byte[254];
    public string host = "127.0.0.1";
    public int port = 9595;
    TcpClient client;
    NetworkStream stream;
    public MediaPlayerCtrl Mplayer;
    API api;
    System.Random rand = new System.Random();
    public int id;
    const int TIMEOUT = 500;
  //  public FileLog logger;

    // Объекты на которых проигрывается видео
    public GameObject FullScreen; // Плоский 
    public MediaPlayerCtrl Full_MediaCtrl;
    public GameObject Sphere; // Сфера
    public string displayType = "sphere";

    public bool Priority = false; // Указываем является ли этот плеер главныым
    public bool isInit = false;


    private LocalSocket _socket;
    private JSonResender _jsonParcer;

    private System.Timers.Timer playerTimer;


    void Start()
    {
        /*
        handler = new SendHandler();
        Thread connect = new Thread(Connect);
        connect.Start();
        storage = new FileStorage(this);
        logger = new FileLog(this);
        id = rand.Next(9999);
        api = new API(this);
        switchPlayer("promo");
        logger.write("Init Player " + id.ToString());
       // return;
         
        Init();
         * */
    


        _socket = new LocalSocket();

        _socket.Connected += socketConnected;

        _jsonParcer = new JSonResender(_socket);
        _jsonParcer.ReciveData += socketParseResponse;

        api = new API(_socket);
        storage = new FileStorage(this);

        storage.MoveUpdateFinished += filmUpdateFinished;


      //  logger = new FileLog(this);
        setIDPlayer();
        switchPlayer("promo");

        new Thread(delegate()
        {
            _socket.SocketConnect("10.0.0.35", port);
        }).Start();
     //   isInit = true;
    }

    private void setIDPlayer()
    {
        id = rand.Next(9999);

        if(api.addInfo==null)
        {
            api.addInfo = new Dictionary<string, string>();
        }
        if(api.addInfo.ContainsKey("playerId"))
        {
            api.addInfo.Remove("playerId");
        }

        api.addInfo.Add("playerId", id.ToString());



    }


    private void loadFilm(string movieKey)
    {
        api.sendFilmLoadStart();

        if (movieKey.Contains("_2d"))
            displayType = "flat";
        else
            displayType = "sphere";

        var pathPhone = storage.Path.phone + movieKey + ".mp4";
        var pathPlugin = storage.Path.plugin + movieKey + ".mp4";
        if (!storage.isMovie(pathPhone))
        {
            api.sendFilmLoadError(FilmError.filmNotFound);
        }
        LoadMovie(pathPlugin);

        getCurrentPlayerObject().OnReady += omMoveLoad;

    }

    private void omMoveLoad()
    {
        api.sendSimpleCommand(messageType.endMoviesLoad);
    }

    private void playLoadedFilm()
    {
        switchPlayer(displayType);
        if (displayType == "sphere")
            Mplayer.Play();
        else if (displayType == "flat")
            Full_MediaCtrl.Play();


        if (playerTimer != null) playerTimer.Stop();
        playerTimer = new System.Timers.Timer();
        playerTimer.Interval = 300;
        playerTimer.Elapsed += new ElapsedEventHandler(OnPlayerTick);
        playerTimer.Enabled = true;
        playerTimer.Start();


        api.sendSimpleCommand(messageType.startMoviePlay);

    }

    private void OnPlayerTick(object source, ElapsedEventArgs e)
    {
        var player = getCurrentPlayerObject();
        if ((player.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING))
        {
            if (Priority)
            {
                int currTime = player.GetSeekPosition();
                api.sendPlayTime(currTime);
            }
        }
        else
        {
            stopLoadedFilm();
        }


    }

    private void stopLoadedFilm()
    {
        var pl = getCurrentPlayerObject();
        if (pl != null)
        {
            pl.Stop();
            if (playerTimer != null) playerTimer.Stop();        
            switchPlayer("promo");
            api.sendSimpleCommand(messageType.endMoviePlay);
        }

    }

    private void ping()
    {
        api.sendPing(Priority);

    }

    private void setPriority(string isAdmin)
    {
        if(isAdmin=="1")
        {
            Priority = true;
        }
        else 
        {
            Priority = false;
        }

    }

    private void readFilmList(string json)
    {
        var list = JsonUtility.FromJson<SerializableMovies>(json);
        storage.CheckAndUploadMovies(list);

    }






    public void socketConnected()
    {
        api.sendSimpleCommand(messageType.connected);
        api.sendSimpleCommand(messageType.initPlayer);
        api.sendSimpleCommand(messageType.getFilmList);
    }

    public void filmUpdateFinished()
    {
        api.sendSimpleCommand(messageType.endMoviesUpdate);

    }

    public void socketParseResponse(string str)
    {
        try
        {
            Dictionary<string, object> data = null;
            var cmd = Json.Deserialize(str) as Dictionary<string, object>;
            if (cmd.ContainsKey("data"))
                data = Json.Deserialize(Json.Serialize(cmd["data"])) as Dictionary<string, object>;


            if (!(!cmd.ContainsKey("playerId")
                || cmd.ContainsKey("playerId") && cmd["playerId"].ToString() == id.ToString()))
            {
                return;
            }

                switch (cmd["act"].ToString())
                {
                    case "load":
                        {
                            var movieKey = data["name"].ToString();
                            loadFilm(movieKey);
                                break;
                        }
                    case "play":
                        {
                            playLoadedFilm();
                            break;
                        }

                    case "stopMoviePlay":
                        {

                            stopLoadedFilm();
                            break;
                        }

                    case "ping":
                        {
                            ping();
                            break;
                        }

                    case "sw":
                        {

                            break;
                        }

                    case "getCurrentPosition":
                        {
                           
                            break;
                        }
                    case "switch":
                        {                          
                            break;
                        }

                    case "setPriority":
                        {
                            var isAdmin = cmd["data"].ToString();
                            setPriority(isAdmin);
                            break;
                        }
                    case "takeFilmList":
                        {
                            readFilmList(Json.Serialize(cmd["data"]));
                            break;
                        }
                    case "getPriority":
                        {
                            
                            break;
                        }
                }
            
        }
        catch (Exception e)
        {
            api.sendRuntimeError(e.Message + " | " + e.StackTrace + "|" + e.InnerException + "|" + e.HelpLink + "|" + e.StackTrace);
        }
    }



    void OnApplicationQuit()
    {
        stream.Close();
        client.Close();
        Debug.Log("Application ending after " + Time.time + " seconds"); Thread.Sleep(0);
    }

    public void switchPlayer(string type)
    {
        try
        {
            var promo = EmptyVideoObject.GetComponent<Renderer>();
            var promoPlayer = EmptyVideoObject.GetComponent<MediaPlayerCtrl>();
            var full = FullScreen.GetComponentInChildren<Renderer>();
            var fullCamera = FullScreen.GetComponentInChildren<Camera>();
            var sphere = Sphere.GetComponentInChildren<Renderer>();
            var sphereCamera = MainCamera.GetComponentInChildren<Camera>();

            if (type == "sphere")
            {
                full.enabled = false;
                fullCamera.enabled = false;
                promo.enabled = false;
                sphere.enabled = true;
                sphereCamera.enabled = true;
            }
            else if (type == "flat")
            {
                full.enabled = true;
                promo.enabled = false;
                sphere.enabled = true;
                fullCamera.enabled = true;
                sphereCamera.enabled = false;
            }
            else if (type == "promo")
            {
                full.enabled = false;
                promo.enabled = true;
                sphere.enabled = false;
                fullCamera.enabled = false;
                promoPlayer.Load(promoPlayer.m_strFileName);
                promoPlayer.Play();
            }

            if (type != "promo")
            {
                promoPlayer.Pause();
                promoPlayer.UnLoad();
            }
        }
        catch (Exception e)
        {
            api.sendRuntimeError(e.Message);
        }
    }

    public void LoadMovie(string fileName)
    {
        try
        {
            var Sphere_mediaCtrl = Sphere.GetComponent<MediaPlayerCtrl>();
            var Promo_mediaCtrl = EmptyVideoObject.GetComponent<MediaPlayerCtrl>();
            if (displayType == "sphere")
            {
                Sphere_mediaCtrl.Load(fileName);
                Promo_mediaCtrl.UnLoad();
            }
            else if (displayType == "flat")
            {
                Sphere_mediaCtrl.UnLoad();
                Full_MediaCtrl.Load(fileName);
            }
            else
            {
                Promo_mediaCtrl.Load(fileName);
                Sphere_mediaCtrl.UnLoad();
                Full_MediaCtrl.UnLoad();
            }
        }
        catch (Exception e)
        {
            api.sendRuntimeError(e.Message + " | " + e.StackTrace + "|" + e.InnerException + "|" + e.HelpLink + "|" + e.StackTrace);
        }
    }


    public FileStorage getFileStorage()
    {
        return storage;
    }



    public MediaPlayerCtrl getCurrentPlayerObject()
    {
        if (displayType == "sphere")
            return Sphere.GetComponentInChildren<MediaPlayerCtrl>();
        else if (displayType == "flat")
            return Full_MediaCtrl;
        else
            return null;
    }


    void Update()
    {
       
    }






























   


   

   
  
}
