﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiniJSON;

namespace fotonotaUtils
{
    class JSonResender : IDisposable
    {
        public delegate void ReciveDataMethodContainer(string data);
        public event ReciveDataMethodContainer ReciveData;


        private string messageString ="";
        private int invalidCounter = 0;


        public JSonResender(LocalSocket ls)
        {
            ls.ReciveData += socketParseResponse;
        }

        public void Dispose()
        {
      
        }

        public void socketParseResponse(string str)
        {
            bool needRepeat = false;
          lock(messageString)
          {
              messageString += str;

              bool isValid = validationAndSend(messageString);
              if(!isValid)
              {
                  invalidCounter++;
                  if (messageString.IndexOf("}{") > 0)
                  {
                      string tmes = messageString.Substring(0, messageString.IndexOf("}{")+1);
                      if (validationAndSend(tmes))
                      {
                          messageString = messageString.Substring(messageString.IndexOf("}{")+1, messageString.Length - messageString.IndexOf("}{")-1);
                          needRepeat = true;
                          invalidCounter = 0;
                      }
                  }
                  else 
                  {
    //                  DebugMessage.Instance.send(messageString,DMmessageType.simple);
                  }
              }else
              {
                  invalidCounter = 0;
                  messageString = "";
              }
               if(invalidCounter>2)
               {
                   invalidCounter = 0;
                   messageString = "";
               }
          }
            if(needRepeat)
            {
                socketParseResponse("");
            }
           
        }


        private bool validationAndSend(string str)
        {
            bool isValid = true;
            try
            {
                var cmd = Json.Deserialize(str) as Dictionary<string, object>;
                if(cmd==null)
                {
                    isValid = false;
                }
            }
            catch (Exception e)
            {
                isValid = false;
            }
            if (isValid)
            {
                if (ReciveData != null)
                    ReciveData(messageString);
            }
            return isValid;
        }


    }
}
