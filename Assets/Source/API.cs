﻿
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System;
using System.Xml;
using System.IO;
using MiniJSON;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System.Threading;
using fotonotaUtils;
using System.Text;


public class FilmError
{
    public const string filmNotFound = "FILM_NOT_FOUND";
}

public class messageType
{
    public const string startMovieLoad = "startMovieLoad";
    public const string filmLoadError = "filmLoadError";
    public const string runtimeError = "runtimeError";
    public const string connected = "Connected";
    public const string startMoviePlay = "startMoviePlay";
    public const string currentPlayTime = "currentPlayTime";
    public const string endMoviePlay = "endMoviePlay";
    public const string ping = "Ping";
    public const string getFilmList = "getFilmList";
    public const string initPlayer = "InitPlayer";
    public const string endMoviesLoad = "endMoviesLoad";
    public const string endMoviesUpdate = "endMoviesUpdate";

}


public class API {
    MediaPlayerCtrl player;
    FileStorage storage;
    GameObject MainCamera;
    public string response = String.Empty;
    public Platform platform;
    public string status;
    public FileLog logger;

    private LocalSocket _socket;
    public Dictionary<string, string>  addInfo = null;

    /*
    public API(Platform platform)
    {
        this.player = platform.Mplayer;
        this.MainCamera = platform.MainCamera;
        this.storage = platform.getFileStorage();
        this.platform = platform;
        logger = platform.logger;
    }
     */

    public API(LocalSocket socket)
    {
        _socket = socket;
    }

    public Thread TheadrCurrentTime;



    /*


    public void CustomParse(string str)
    {
        Debug.Log("Customer");
        string[] delimiter = new string[] { "}{" };
        var te1  = str.Substring(str.IndexOf("}{") + 1, str.Length - str.IndexOf("}{") - 1);
        Debug.Log("te1 " + str);
        for (int i = 0; i < str.Length; i++)
        {
            Debug.Log("test:" + str[i]);
        }
        //.join("},{") + ']'
        
    }
    
    public void routes(string json)
    {
        try
        {
            Dictionary<string, object> data = null;
            var cmd = Json.Deserialize(json) as Dictionary<string, object>;
            if (cmd.ContainsKey("data"))
                data = Json.Deserialize(Json.Serialize(cmd["data"])) as Dictionary<string, object>;



            if (!cmd.ContainsKey("playerId") 
                || cmd.ContainsKey("playerId") && cmd["playerId"].ToString() == platform.id.ToString())
            {

                switch (cmd["act"].ToString())
                {
                    case "load":
                        {
                            try
                            {

                                var movieKey = data["name"].ToString();
                                var response = setResponse("startMovieLoad", true);
                                platform.Send(response);


                                var pathServer = storage.Path.HttpServer + movieKey;

                                if (movieKey.Contains("_2d"))
                                    platform.displayType = "flat";
                                else
                                    platform.displayType = "sphere";

                                var pathPhone = storage.Path.phone + movieKey + ".mp4";
                                var pathPlugin = storage.Path.plugin + movieKey + ".mp4";
                                Debug.Log(pathPlugin);
                                Debug.Log(pathPhone);
                                if (!storage.isMovie(pathPhone))
                                {

                                    response += "Not movie file.";
                                    break;
                                }
                                platform.LoadMovie(pathPlugin);
                                Thread isLoad = new Thread(thrIsLoad);
                                isLoad.Start();
                                break;
                            }
                            catch (Exception e)
                            {
                                logger.write(e.Message + e.StackTrace);
                                break;
                            }
                         }
                    case "play":
                        {
                            play();
                            TheadrCurrentTime = new Thread(platform.EventPlayerCurrentTime);
                            TheadrCurrentTime.Start();
                            break;
                        }

                    case "stopMoviePlay":
                        {
                            var pl = platform.getCurrentPlayerObject();
                            if (pl != null)
                            {
                                pl.Stop();
                                setResponse("stopMoviePlay");
                                platform.switchPlayer("promo");
                            }

                            break;
                        }

                    case "ping":
                        {
                            setResponse("Ping");
                            break;
                        }

                    case "sw":
                        {
                            if (platform.displayType == "sphere")
                            {
                                platform.switchPlayer("flat");
                                platform.displayType = "flat";
                            }
                            else
                            {
                                platform.switchPlayer("sphere");
                                platform.displayType = "sphere";
                            }
                            setResponse("Ping");
                            break;
                        }

                    case "getCurrentPosition":
                        {
                            var pos = platform.getCurrentPlayerObject().GetSeekPosition() / 100;
                            setResponse(pos.ToString() + " ms");
                            break;
                        }
                    case "switch":
                        {
                            platform.displayType = data["type"].ToString();
                            break;
                        }

                    case "setPriority":
                        {
                            if (cmd.ContainsKey("playerId"))
                            {
                                platform.Priority = true;
                                setResponse("setPriority");

                            }
                            break;
                        }
                    case "getPriority":
                        {
                            if(platform.Priority)
                                platform.Send("GetPriority_True");
                            break;
                        }
                }
            }
        }
        catch(Exception e)
        {
            setResponse( e.StackTrace + " " + e.Message );
        }

    }

    public void thrIsLoad()
    {

        while(platform.getCurrentPlayerObject().GetCurrentState() != MediaPlayerCtrl.MEDIAPLAYER_STATE.READY)
        {
            Thread.Sleep(200);
        }
        response = formatResponse("endMoviesLoad");
        platform.Send(response);
    }

    public void play()
    {
        platform.switchPlayer(platform.displayType);
        if (platform.displayType == "sphere")
            player.Play();
        else if (platform.displayType == "flat")
            platform.Full_MediaCtrl.Play();

        //platform.handler.onPlay();
        setResponse("startMoviePlay");
    }

    public string setResponse(string eventName, bool isReturn = false)
    {
        var tmpResponse = "{\"act\":\"" + eventName + "\",\"playerId\":\"" + platform.id + "\"}";

        if (isReturn == true)
            return tmpResponse;

        return response = tmpResponse;
    }

    public string formatResponse(string eventName) {
        var tmpResponse = "{\"act\":\"" + eventName + "\",\"playerId\":\"" + platform.id + "\"}";
        return tmpResponse;
    }

    public string getResponse()
    {
        return response;
    }


    public string setPriority(int id, int isAdmin)
    {
        if(isAdmin == 1)
            platform.Priority = true;
        return setResponse("setPriority", true);
    }

    */





    public void sendFilmLoadStart()
    {
        sendSimpleCommand(messageType.startMovieLoad);
    }


    public void sendFilmLoadError(string error)
    {
        Dictionary<string, string> messageObj = new Dictionary<string, string>();

        messageObj.Add("act", messageType.filmLoadError);
        messageObj.Add("error", error);

        sendLineCommand(messageObj);
    }

    public void sendPlayTime(int time)
    {
        Dictionary<string, string> messageObj = new Dictionary<string, string>();

        messageObj.Add("act", messageType.currentPlayTime);
        messageObj.Add("position", time.ToString());

        sendLineCommand(messageObj);
    }

    public void sendPing(bool isPriority)
    {
        Dictionary<string, string> messageObj = new Dictionary<string, string>();

        messageObj.Add("act", messageType.currentPlayTime);
        messageObj.Add("priority", isPriority ? "1" : "0");

        sendLineCommand(messageObj);
    }


    public void sendRuntimeError(string error)
    {
        Dictionary<string, string> messageObj = new Dictionary<string, string>();

        byte[] bytesToEncode = Encoding.UTF8.GetBytes(error);
        string encodedText = Convert.ToBase64String(bytesToEncode);

        messageObj.Add("act", messageType.runtimeError);
        messageObj.Add("error", encodedText);

        sendLineCommand(messageObj);
    }



    public void sendLineCommand(Dictionary<string, string> messageObj)
    {
        Dictionary<string, string> sendObj = new Dictionary<string, string>();

        if(addInfo!= null)
        {
            foreach (KeyValuePair<string, string> kvp in addInfo)
            {

                sendObj.Add(kvp.Key, kvp.Value);
            }
        }


        foreach (KeyValuePair<string, string> kvp in messageObj)
        {

            sendObj.Add(kvp.Key, kvp.Value);
        }

        _socket.SendJson(Json.Serialize(sendObj));


    }

    public void sendSimpleCommand(string command)
    {
        Dictionary<string, string> messageObj = new Dictionary<string, string>();

        messageObj.Add("act", command);
        sendLineCommand(messageObj);
    }





}
