﻿using System;
using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Threading;



namespace fotonotaUtils
{
    public class LocalSocket : IDisposable
    {
        readonly Object _reciveSyncRoot = new Object(); // for sync with timer threads only


        private System.Timers.Timer updateStateTimer;

        public Socket socket;
        private string _host;
        private int _port;
        public bool _connected = false;

        private ManualResetEvent _receiveDone = new ManualResetEvent(false);
        private ManualResetEvent _sendDone = new ManualResetEvent(false);
        private static ManualResetEvent _connectDone = new ManualResetEvent(false);




        public delegate void ConnectedMethodContainer();
        public delegate void ReciveDataMethodContainer(string data);
        public delegate void ReciveByteDataMethodContainer(byte[] data);

        public event ConnectedMethodContainer Connected;
        public event ReciveDataMethodContainer ReciveData;
        public event ReciveByteDataMethodContainer ReciveByteData;


        public LocalSocket()
        {

 
        }



        public void SocketConnect(string host, int port, bool async = true)
        {
            _host = host;
            _port = port;



            socket = null;
            //    Console.WriteLine("Socket connect...");

            while (socket == null)
            {
                socket = ConnectSocket(_host, _port);
                if (socket == null)
                {
                    Thread.Sleep(2000);
                }
            }

            if (socket != null && socket.Connected)
            {

                Console.WriteLine("DeviceController connected to ServerSocket");

                _connectDone.Set();
                _connected = true;
                if (Connected != null)
                    Connected();

                
                if (async)
                {
                    Receive(socket);
                }
              
            }


        }



        private Socket ConnectSocket(string server, int port)
        {
            IPHostEntry hostEntry = null;

            // Get host related information.
            hostEntry = Dns.GetHostEntry(server);
            IPEndPoint ipe = new IPEndPoint(IPAddress.Parse(server), port);
            Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                tempSocket.Connect(ipe);
            }
            catch (SocketException e)
            {

                Console.WriteLine("SocketException source: {0}", e);

            }

            if (tempSocket.Connected)
            {
                return tempSocket;
            }
            else
            {
                return null;
            }
            //  }
        }

        private void Receive(Socket client)
        {
            try
            {
                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = client;

                // Begin receiving the data from the remote device.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {

            lock (_reciveSyncRoot)
            {
                try
                {


                    // Retrieve the state object and the client socket 
                    // from the asynchronous state object.
                    StateObject state = (StateObject)ar.AsyncState;
                    Socket client = state.workSocket;



                    // Read data from the remote device.
                    int bytesRead = client.EndReceive(ar);


                    if (bytesRead > 0)
                    {
                        // There might be more data, so store the data received so far.
                        state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));


                      



                        // rrrrrrrrrrrr end of stream??????

                        String response = String.Copy(state.sb.ToString());

                        state.sb = new StringBuilder();

                        _receiveDone.Set();

                        client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);

                        if (ReciveData != null)
                            ReciveData(response);



                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }


        public void SendData(String data)
        {
            if ((_connected == false) || (socket == null)) return;
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            try{
            socket.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), socket);
            }
            catch(System.Net.Sockets.SocketException er)
            {
                socket = null;
                _connected = false;

            }
        }


        public void SendJson(string json)
        {
            Dictionary<string, string> messageObj = new Dictionary<string, string>();

            /*
            messageObj.Add("act", "serialData");
            messageObj.Add("device", "cinemaControlPanel");


            Dictionary<string, string> paramsObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(json);

            foreach (KeyValuePair<string, string> kvp in paramsObj)
            {

                messageObj.Add(kvp.Key, kvp.Value);
            }


            string messageJSON = "";

            try
            {
                messageJSON = new JavaScriptSerializer().Serialize(messageObj);

                Console.WriteLine(messageJSON);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            */
            SendData(json);


        }



        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);

                // Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                _sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }



        public void Dispose()
        {
            if (socket.Connected)
            {
                socket.Close();
            }
        }



    }

    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.

        public const int BufferSize = 8192;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }
}


